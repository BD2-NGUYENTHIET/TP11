set serveroutput on;

drop table archive;
create table archive (
    old_ids int,
    old_idc int,
    old_idv int,
    old_jour int,
    old_avoir int
);


create or replace trigger archivage 
    after delete
    on sejour
    for each row
    declare
        n int;
    begin
        select avoir into n
            from client
            where idc = :old.idc;

        insert into archive values(
            :old.ids,
            :old.idc,
            :old.idv,
            :old.jour,
            n
            );

        dbms_output.put_line(:old.ids || ' ' || :old.idc || ' ' || :old.idv || ' ' || :old.jour || ' ' || n);
    end;
/


declare 
    le_nb int;
begin
    traitement3(0,le_nb);
    dbms_output.put_line(le_nb);
end;
/


for each row il faut before 

create or replace trigger constr 
    after insert
    on sejour
    declare 
        cursor c is
        select client.idc, nom, avoir, sum(prix) as depenses 
            from client, village, sejour
            where client.idc = sejour.idc
            and sejour.idv = village.idv
            group by client.idc, client.nom, avoir
                having avoir + sum(prix) > 2000;
        res c%rowtype;
    begin
        open c;
        fetch c into res;
        if c%found then
            raise_application_error(-20002, 'mechant : ' || res.idc);
        end if;
    end;
/

create or replace trigger constr
    before insert 
    on sejour
    for each row
    declare
        le_prix sejour.prix%type;
        depense int;
        l_avoir client.avoir%type;
    begin
        select avoir from client where idc = :new.idc;
        select sum(prix) into depense
            from sejour,village
            where idc = :new.idc
            and village.idv = sejour.idv;
        select prix into le_prix
            from village
            where village.idv = :new.idv;



        if l_avoir+depense+le_prix > 2000 then
            raise_application_error(-20002, 'mechant : ' || :old.idc);
        end if;
    end;
/
    
insert into sejour values(99999,1,13,365);

